<?php
    // Menu Registration
    register_nav_menus(array(
        'primary_nav' => 'Main Header Navigation',
        'footer_nav' => 'Footer Navigation'
    ));

    // Side bar Registration
    add_action( 'widgets_init', 'regsiter_theme_sidebars' );
    function regsiter_theme_sidebars() {
        register_sidebar(
            array(
                'id' => 'primary',
                'name' => __( 'Primary' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
    }
    
    /* ======================================================================
	 * Image-URL-Default.php
	 * Overrides default image-URL behavior
	 * http://wordpress.org/support/topic/insert-image-default-to-no-link
	 * ====================================================================== */

	update_option('image_default_link_type','none');
	
	/* Add a custom field to the field editor (See editor screenshot) */
	add_action("gform_field_standard_settings", "my_standard_settings", 10, 2);

	function my_standard_settings($position, $form_id){

	// Create settings on position 25 (right after Field Label)

	if($position == 25){
	?>
		
	<li class="admin_label_setting field_setting" style="display: list-item; ">
	<label for="field_placeholder">Placeholder Text

	<!-- Tooltip to help users understand what this field does -->
	<a href="javascript:void(0);" class="tooltip tooltip_form_field_placeholder" tooltip="&lt;h6&gt;Placeholder&lt;/h6&gt;Enter the placeholder/default text for this field.">(?)</a>
			
	</label>
		
	<input type="text" id="field_placeholder" class="fieldwidth-3" size="35" onkeyup="SetFieldProperty('placeholder', this.value);">
		
	</li>
	<?php
	}
	}

	/* Now we execute some javascript technicalitites for the field to load correctly */

	add_action("gform_editor_js", "my_gform_editor_js");

	function my_gform_editor_js(){
	?>
	<script>
	//binding to the load field settings event to initialize the checkbox
	jQuery(document).bind("gform_load_field_settings", function(event, field, form){
	jQuery("#field_placeholder").val(field["placeholder"]);
	});
	</script>

	<?php
	}

	/* We use jQuery to read the placeholder value and inject it to its field */

	add_action('gform_enqueue_scripts',"my_gform_enqueue_scripts", 10, 2);

	function my_gform_enqueue_scripts($form, $is_ajax=false){
	?>
	<script>

	jQuery(function(){
	<?php

	/* Go through each one of the form fields */

	foreach($form['fields'] as $i=>$field){

	/* Check if the field has an assigned placeholder */
			
	if(isset($field['placeholder']) && !empty($field['placeholder'])){
				
	/* If a placeholder text exists, inject it as a new property to the field using jQuery */
				
	?>
				
	jQuery('#input_<?php echo $form['id']?>_<?php echo $field['id']?>').attr('placeholder','<?php echo $field['placeholder']?>');
				
	<?php
	}
	}
	?>
	});
	</script>
	<?php
	}

	
    // Misc registrations
    add_theme_support('post-thumbnails');
    add_image_size("list-thumb", 264, 264, TRUE);

    // ShortenText
	function ShortenText($text) { // Function name ShortenText
        $chars_limit = 25; // Character length
        $chars_text = strlen($text);
        $text = $text." ";
        $text = substr($text,0,$chars_limit);
        $text = substr($text,0,strrpos($text,' '));

        if ($chars_text > $chars_limit)
        { $text = $text."..."; } // Ellipsis
        return $text;
	}


    // Menu Walker Class
    class themeslug_walker_nav_menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus
    function start_lvl( &$output, $depth ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu uk-nav uk-nav-navbar',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
            );
        $class_names = implode( ' ', $classes );

        // build html
        $output .= "\n" . $indent . '<div class="uk-dropdown uk-dropdown-navbar"><ul class="' . $class_names . '">' . "\n";
    }

    // add main/sub classes to li's and links
     function start_el( &$output, $item, $depth, $args ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        if(strpos($class_names, 'menu-item-has-children')) {$haschild = "data-uk-dropdown"; }
        // build html
        $output .= $indent . '<li '. $haschild .' id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}