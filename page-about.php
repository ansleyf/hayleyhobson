<?php
	get_header("simple");
?>
	<div class="about">
	<?php
		while(have_posts()) : the_post();
		$row1 = get_post_meta($post->ID, "Row One");
		$row2 = get_post_meta($post->ID, "Row Two");
		$footer = get_post_meta($post->ID, "Footer Type", true);
	?>

		<div class="uk-grid uk-grid-preserve about-t" data-uk-grid-match="{row: true}">
			<div class="uk-width-medium-1-2 notext" style="background: url('<?php echo wp_get_attachment_url($row1[0]); ?>');">
			</div>
			<div class="uk-width-medium-1-2">
				<div class="container">
					<h2><?php echo $row1[1]; ?></h2>
					<?php echo $row1[2]; ?>
					<div class="uk-margin-top">
						<a class="cta dark" href="<?php echo $row1[4]; ?>"><?php echo $row1[3]; ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-grid uk-grid-preserve about-t uk-margin-top-remove about-t-2" data-uk-grid-match="{row: true}">
			<div class="uk-width-medium-1-2 hasbg">
				<div class="container">
					<h2><?php echo $row2[1]; ?></h2>
					<?php echo $row2[2]; ?>
					<?php if($row2[3] != "") : ?>
					<div class="uk-text-center uk-margin-top">
						<a class="cta white" href="<?php echo $row2[4]; ?>"><?php echo $row2[3]; ?></a>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="uk-width-medium-1-2 notext" style="background: url('<?php echo wp_get_attachment_url($row2[0]); ?>');">
			</div>
		</div>
		<div class="uk-grid about-p container uk-margin-large-top uk-margin-large-bottom">
			<div class="uk-width-medium-1-1">
				<?php the_post_thumbnail("full",array("class"=>"uk-align-left uk-margin-large-right")); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<div class="uk-margin-large-top uk-text-center">
					<a class="cta" href="/work-with-hayley">Work With Hayley</a>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php
	get_footer($footer);