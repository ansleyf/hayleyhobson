<?php
	get_header();
?>
	<div class="uk-container uk-container-center uk-margin-large-top">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h1 class="uk-article-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
			<?php $blogcontent = get_post_meta($post->ID, "Posts to Show"); ?>
			<?php wp_reset_query(); ?>
			<?php if (!$blogcontent) { echo ""; } else { ?>
				<?php $args = array(
						'cat' => $blogcontent[0],
						'posts_per_page' => 4
				);
				$query = new WP_Query( $args ); ?>
				<hr class="uk-margin-large-top uk-margin-large-bottom" style="clear:both;"/>
                    		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part("loop","view"); ?>		
				<?php endwhile; ?>
				
			<?php } ?>

			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer($footer);