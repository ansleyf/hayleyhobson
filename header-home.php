<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_template_directory_uri(); ?>/css/base.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css" />
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/base.js"></script>

		<script type="text/javascript" src="//use.typekit.net/unn2kyf.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-54331657-1', 'auto');
			ga('send', 'pageview');
		</script>
		<meta name="google-site-verification" content="kcXxTCaGgU1NaB3sJAgrvEu79ve_XxJnGaLs5RkDFAc" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <header>
		<div class="uk-width-1-1 site-header large">
			<?php get_template_part("common", "header"); ?>
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="uk-width-large-6-10 uk-push-4-10 container">
						<h1>GOT WHOLE YOU?</h1>
						<h2>Get the Recipes, Wellness Tips, and</h2>
						<h2><span>Inspiration</span></h2>
    					<h2 class="optin-text-special">that will get you there.</h2>
						<form accept-charset="UTF-8" action="https://fm192.infusionsoft.com/app/form/process/21c44e8b39957d9e47b7463ceec55b52" class="infusion-form uk-form uk-width-1-1 uk-align-center" method="POST">
						<input name="inf_form_xid" type="hidden" value="21c44e8b39957d9e47b7463ceec55b52" />
   					 	<input name="inf_form_name" type="hidden" value="Sign up for newsletter" />
    						<input name="infusionsoft_version" type="hidden" value="1.32.0.71" />
							<fieldset>
							<div class="uk-form-icon infusion-field">
								<i class="uk-icon uk-icon-user"></i>
								<input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="first name"/>
							</div>
							<div class="uk-form-icon infusion-field">
								<i class="uk-icon uk-icon-envelope"></i>
								<input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="email" placeholder="email address"/>
							</div>
								<input type="submit" value="yes!" />
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1 subnav">
			<div class="uk-container uk-container-center">
				<ul class="uk-subnav uk-hidden-small">
					<li><a href="<?php echo get_permalink(9068); ?>">WHOLE HOME & LIVING</a></li>
					<li><a href="<?php echo get_permalink(9070); ?>">WHOLE FOOD & LIFESTYLE</a></li>
					<li><a href="<?php echo get_permalink(9072); ?>">WHOLE WORK & LIFE BALANCE</a></li>
					<li><a href="<?php echo get_permalink(9074); ?>">WHOLE HEALTH & WELLNESS</a></li>
				</ul>
			</div>
		</div>
    </header>