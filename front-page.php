<?php get_header("home"); ?>
	<section class="wholePillars">
	<?php $heading = get_post_meta(9047, "Heading"); ?>
		<div class="uk-container uk-container-center">
			<h2><span><?php echo $heading[0]; ?></span></h2>
			<p class="intro">
				<?php echo $heading[1]; ?>
			</p>
			<?php $video = do_shortcode('[responsive_vid]'); if (!$video) { echo ""; }  else { echo $video; } ?>
			<div class="uk-grid uk-margin-large-top">
				<div class="uk-width-medium-1-4">
				<?php $column1 = get_post_meta(9047, "Column One"); ?>
					<div class="uk-panel">
						<a href="/whole-home-living"><img class="uk-align-center" src="<?php echo wp_get_attachment_url($column1[0]); ?>" />
						<h3 class="uk-margin-bottom">Whole<div><?php echo $column1[1]; ?></div></h3></a>
						<?php echo $column1[2]; ?>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
				<?php $column2 = get_post_meta(9047, "Column Two"); ?>
					<div class="uk-panel">
						<a href="/whole-food-lifestyle"><img class="uk-align-center" src="<?php echo wp_get_attachment_url($column2[0]); ?>" />
						<h3 class="uk-margin-bottom">Whole<div><?php echo $column2[1]; ?></div></h3></a>
						<?php echo $column2[2]; ?>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
				<?php $column3 = get_post_meta(9047, "Column Three"); ?>
					<div class="uk-panel">
						<a href="/whole-work-life-balance"><img class="uk-align-center" src="<?php echo wp_get_attachment_url($column3[0]); ?>" />
						<h3 class="uk-margin-bottom">Whole<div><?php echo $column3[1]; ?></div></h3></a>
						<?php echo $column3[2]; ?>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
				<?php $column4 = get_post_meta(9047, "Column Four"); ?>
					<div class="uk-panel">
						<a href="/whole-health-wellness"><img class="uk-align-center" src="<?php echo wp_get_attachment_url($column4[0]); ?>" />
						<h3 class="uk-margin-bottom">Whole<div><?php echo $column4[1]; ?></div></h3></a>
						<?php echo $column4[2]; ?>
					</div>
				</div>
				<div class="uk-width-1-1 uk-margin-large-top uk-text-center">
					<a class="uk-button" href="<?php echo $heading[2]; ?>">Get Started <i class="uk-icon uk-icon-rocket"></i></a>
				</div>
			</div>
		</div>
	</section>
	<section class="meetHayley">
		<div class="uk-container uk-container-center">
			<?php
				$meet = new WP_Query("page_id=9054");
				while($meet->have_posts()) : $meet->the_post(); 
			?>
			<div class="uk-grid">
				<div class="uk-width-medium-1-2 uk-push-1-2">
					<h2><?php the_title(); ?></h2>
					<div class="content">
						<?php the_content(); ?>
					</div>
					<a class="uk-button" href="<?php echo get_post_meta(get_the_ID(), "Meet", true); ?>">MORE ABOUT HAYLEY</a>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</section>
<?php get_footer();