<article class="uk-article">
	<a href="<?php the_permalink(); ?>">
		<div class="uk-article-title"><?php the_title(); ?></div>
	</a>
	<div class="uk-article-meta"><?php the_time(get_option("date_format")); ?></div>
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail("list-thumb", array("class"=>"uk-align-left")); ?>
	</a>
	<?php the_excerpt(); ?>
	<a class="more" href="<?php the_permalink(); ?>">READ MORE</a>
</article>