	<div id="offcanv" class="uk-offcanvas">
		<div class="uk-offcanvas-bar uk-offcanvas-bar">
			<?php 
				$offcanvnav = array(
					"container"			=> "",
					"theme_location"	=> "primary_nav",
					"menu_class"		=> "uk-nav uk-nav-offcanvas",
					"menu_id"			=> ""
				);
				wp_nav_menu($offcanvnav);
			?>
		</div>
	</div>
	<nav class="uk-navbar uk-navbar-attached uk-width-1-1">
		<div class="uk-container uk-container-center">
			<div class="uk-navbar-content">
				<a href="#offcanv" class="uk-navbar-toggle uk-hidden-large" data-uk-offcanvas></a>
			</div>
			<div class="uk-navbar-content uk-navbar-center">
				<a href="<?php echo home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a>
			</div>
			<ul class="uk-navbar-nav left uk-visible-large">
				<li><a href="<?php echo get_the_permalink(9058); ?>">About Me</a></li>
				<li><a href="<?php echo get_the_permalink(3321); ?>">Blog</a></li>
				<li><a href="<?php echo get_the_permalink(7274); ?>">Essential Oils</a></li>
			</ul>
			<div class="uk-navbar-content uk-navbar-flip uk-hidden-small">
				<a href="http://www.facebook.com/hayleyhobsonhealthcoach" target="_blank"><i class="uk-icon uk-icon-facebook uk-icon-button"></i></a>
				<a href="http://twitter.com/hayleyhobson" target="_blank"><i class="uk-icon uk-icon-twitter uk-icon-button"></i></a>
				<a href="http://instagram.com/hayleyhobson" target="_blank"><i class="uk-icon uk-icon-instagram uk-icon-button"></i></a>
				<a href="http://pinterest.com/hayleyhobson69/" target="_blank"><i class="uk-icon uk-icon-pinterest uk-icon-button"></i></a>
				<a href="http://www.youtube.com/watch?v=AhfNsc8pxQY" target="_blank"><i class="uk-icon uk-icon-youtube uk-icon-button"></i></a>
			</div>
			<ul class="uk-navbar-nav uk-navbar-flip uk-visible-large">
				<li><a href="<?php echo get_the_permalink(1847); ?>">Work With Hayley</a></li>
			</ul>
		</div>
	</nav>