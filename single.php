<?php
	get_header();
?>
	<div class="uk-container uk-container-center blogView uk-margin-large-top">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			<?php
				while(have_posts()) : the_post();
					get_template_part("single","view");
				endwhile;
				if (function_exists('nrelate_related')) nrelate_related();
				?>
				<h2 class="comments">Comments</h2>
				<?php comments_template(); ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();