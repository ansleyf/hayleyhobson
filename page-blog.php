<?php
	get_header();
?>
	<div class="uk-container uk-container-center blogView uk-margin-large-top">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			<!-- <h1>Blog</h1> -->
			<?php 
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$args = array(
					"post_type" => "post",
					"paged"		=> $paged
				);
				$wp_query = new WP_Query($args);
				while($wp_query->have_posts()) : $wp_query->the_post();
			?>
				<?php get_template_part("loop","view"); ?>
			<?php endwhile; ?>
			<ul class="uk-pagination uk-margin-top uk-margin-bottom">
				<li class="uk-pagination-previous"><?php previous_posts_link("Newer Entries"); ?></li>
				<li class="uk-pagination-next"><?php next_posts_link("Older Entries"); ?></li>
			</ul>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer(); 