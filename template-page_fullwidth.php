<?php
/*
	Template Name: Full Width
*/
	get_header();
?>
	<div class="uk-container uk-container-center uk-margin-large-top fullwidth">
		<div class="uk-grid">
			<div class="uk-width-1-1">
			<?php while(have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h1 class="uk-article-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php
	get_footer($footer);