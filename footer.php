<div class="footer-nav">
	<div class="uk-container uk-container-center">
	<?php wp_nav_menu( array( 'theme_location' => 'footer_nav', 'menu_class' => 'footer-navigation' ) ); ?>
	</div>
</div>
<section class="change">
	<div class="uk-container uk-container-center">
		<div class="uk-width-medium-2-3 uk-container-center">
			<h2>Are You Ready for a Change?</h2>
			<h3>Get in touch to discover YOUR BEST SELF</h3>
			<form accept-charset="UTF-8" class="uk-form" action="https://fm192.infusionsoft.com/app/form/process/e6fefaeffa597b8896e7125d73f2d3a3" method="POST">
<input name="inf_form_xid" type="hidden" value="e6fefaeffa597b8896e7125d73f2d3a3" />
<input name="inf_form_name" type="hidden" value="Website &#a;Contact Form" />
<input name="infusionsoft_version" type="hidden" value="1.33.0.46" />
				<div class="uk-grid" data-uk-margin>
					<div class="uk-width-medium-1-3"><input id="inf_field_FirstName" name="inf_field_FirstName" type="text" class="uk-width-1-1 clear dark" placeholder="Name" /></div>
				<div class="uk-width-medium-1-3"><input id="inf_field_Email" name="inf_field_Email" type="text" class="uk-width-1-1 clear dark" placeholder="Email Address" /></div>
				<div class="uk-width-medium-1-3"><input class="uk-width-1-1 clear dark infusion-field-input-container" id="inf_field_Phone1" name="inf_field_Phone1" type="text" placeholder="Phone Number" /></div>
					<!--<div class="uk-width-1-1 uk-margin-top"><textarea id="inf_custom_Message" name="inf_custom_Message" class="uk-width-1-1 clear dark" placeholder="Message"></textarea></div>-->
					<div class="uk-width-1-1 uk-margin-top"><input type="submit" class="uk-button" value="I'M READY!" /></div>
				</div>
			</form>
		</div>
	</div>
</section>
<footer class="uk-width-1-1">
	<div class="af">
		<div class="uk-text-center uk-text-small">
			&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; <a href="http://ansleyfones.com/">Design &amp; Development by <img src="<?php echo get_template_directory_uri(); ?>/images/af-logo-very-small.png" alt="AF" /> Ansley Fones</a>
		</div>
	</div>
</footer>
<script type="text/javascript">//<![CDATA[
            // Google Analytics for WordPress by Yoast v4.3.5 | http://yoast.com/wordpress/google-analytics/
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-39202713-1']);
				            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            //]]></script>
            
<?php wp_footer(); ?>
</body>
</html>