<article class="uk-article">
	<a href="<?php the_permalink(); ?>">
		<div class="uk-article-title"><?php the_title(); ?></div>
	</a>
	<div class="uk-article-meta"><?php the_time(get_option("date_format")); ?></div>
	<?php the_content(); ?>
	<div class="uk-grid uk-margin-large-top uk-margin-large-bottom" data-uk-grid-margin>
		<div class="uk-width-medium-2-4 uk-float-left">
			<img src="<?php echo get_template_directory_uri(); ?>/images/postsignature.png" />
		</div>
		<div class="uk-width-medium-2-4 uk-float-right">
			<?php echo do_shortcode("[social_buttons]"); ?>
		</div>
	</div>
</article>